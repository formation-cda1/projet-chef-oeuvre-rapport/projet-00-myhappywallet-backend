import { Result, ResultCode }  from './../../utils/results/';
import { ErrorException,ErrorCode }  from "./../../utils/errors/";
import {
  NODE_ENV,
  SENDGRID_API_KEY,
  // REGISTER_TOKEN,
  EMAIL_SENDER,
} from "./../../config/config";
// On va utiliser notre ORM pour modifier notre BDD (couche de persistence)
//script "générale" utilisable par notre service lié aux Users
import { createUserProps } from "../../utils/validators/register.validator";
import sgMail from "@sendgrid/mail";

export class UserRepo {
  private entities: any;
  private emailExist: boolean;
  private resetTokenExist: boolean;
  private isVerified:boolean;

  constructor(entities: any) {
    this.entities = entities;
  }

  public async create(userProps: createUserProps) {
    const UserEntity = this.entities.utilisateur;

    console.log("dans UserRepo create fctn", userProps);

    const user = await UserEntity.create({
      data: {
        email: userProps.email,
        password: userProps.password,
        firstname: userProps.firstname,
        lastname: userProps.lastname,
      },
    });

    return user
  }

  public async delete(email: string,userId:number) {
    const UserEntity = this.entities.utilisateur;

    console.log("dans UserRepo delete fctn");

    await UserEntity.deleteMany({
      where: { email: email,id:userId }
    });

    return await new Result(ResultCode.Deleted,`User with ${email} account`).response_delete()
  }
  public async resetPassword(email: string, resetToken:string, resetTokenExpiration:Date) {
    const UserEntity = this.entities.utilisateur;

      const user = await UserEntity.findUnique({
        where: { email: email },
      }).catch((err:any) => {console.log("Inside Prisma",err) ;throw new ErrorException(ErrorCode.PrismaError,"Account email not found , you can register")});

      await UserEntity.update({
        where: {
          id: user.id,
        },
        data: {
          resetToken: resetToken,
          resetTokenExpiration:resetTokenExpiration ,
        },
      }).catch((err:any) => {console.log("Inside Prisma",err);throw new ErrorException(ErrorCode.PrismaError)});
      
      return await new Result(ResultCode.Created,`ResetToken successfully added`).response_post()
      // return {
      //   success: true,
      //   message: `ResetToken successfully added`,
      // };
  }

  public async newPassword(newpassword: string, resetToken: string) {
    const UserEntity = this.entities.utilisateur;
    // const result = await UserEntity.findMany({
    //   where: { resetToken: resetToken },
    //   select:{resetTokenExpiration:true}
    // });

    const result = await UserEntity.findMany({
      where: {
        resetToken: resetToken,
        resetTokenExpiration: {
          gte: new Date() /* Includes time offset for UTC */,
        },
      },
    })
    if (result.length === 0 || result[0]==undefined ){
      throw new ErrorException(ErrorCode.Unauthorized)
    }

    const user = result[0]
    console.log("user found by resetToken", user);
    if (user.length > 1){
      throw new ErrorException(ErrorCode.Unauthorized,"Reset Token is expired")
    }

    await UserEntity.updateMany({
      where: {
        id: user.id,
      },
      data: {
        password: newpassword,
        resetToken: null,
        resetTokenExpiration: null,
      },
    });

    return await new Result(ResultCode.Created,`New password created`).response_post()
    // return { success: true, message: `New password created` };
  }

  public async confirmRegistration(id: string) {
    const UserEntity = this.entities.utilisateur;
    console.log(
      await UserEntity.update({
        where: {
          id: parseInt(id),
        },
        data: {
          verified: true,
        },
      })
    );
    
    const result = await new Result(ResultCode.Created, `Registration User is successfull`).response_get()
    return result
    // return {
    //   success: true,
    //   message: `Registration User ${user.email} is successfull`,
    // };
  }

  public async exists(email: string): Promise<boolean> {
    const UserEntity = this.entities.utilisateur;

    // console.log('email dans findUnique', email)
    const result = await UserEntity.findUnique({ where: { email: email } });

    if (result === null) {
      this.emailExist = false;
    } else {
      this.emailExist = true;
    }
    return this.emailExist;
  }

  public async getUserByEmail(email: string) {
    const UserEntity = this.entities.utilisateur;

    const result = await UserEntity.findUnique({ where: { email: email } });

    return result;
  }
  public async getUserById(id: number) {
    const UserEntity = this.entities.utilisateur;

    const result = await UserEntity.findUnique({ where: { id: id } });

    return result;
  }

  public async existUserResetToken(resetToken: string) {
    const UserEntity = this.entities.utilisateur;

    const result = await UserEntity.findMany({
      where: { resetToken: resetToken },
    });
    console.log("Check resetToken", result);
    console.log(result === null || result == []);
    if (result === null || result.length === 0 ) {
      this.resetTokenExist = false;
    } else {
      this.resetTokenExist = true;
    }
    return this.resetTokenExist;
  }


  public async isUserAccountVerified(email: string) {
    const UserEntity = this.entities.utilisateur;

    const result = await UserEntity.findUnique({ where: { email: email } });
    if (result === null) {
      this.isVerified = false;
    } else {
      this.isVerified = result.verified;
    }
    return this.isVerified;
  }

  public async sendMail(email: string, subject: string, text: string) {
    console.log("await sending email confirmation");
    sgMail.setApiKey(SENDGRID_API_KEY as string);
    let trackingFalse: boolean = false;
    if (NODE_ENV === "production") {
      trackingFalse = true;
    } else {
      trackingFalse = false;
    }
    const msg = {
      to: email, // Change to your recipient
      from: EMAIL_SENDER as string, // Change to your verified sender
      subject: subject,
      // text:text,
      html: text,
      trackingSettings: {
        clickTracking: {
          enable: trackingFalse,
          enableText: trackingFalse,
        },
        openTracking: {
          enable: trackingFalse,
        },
      },
    };

    const isEmailSent: Promise<boolean> = sgMail
      .send(msg)
      .then(async (response) => {
        console.log("RESPONSE MAIL", response[0].statusCode);
        console.log("RESPONSE HEADER", response[0].headers);
        if (response[0].statusCode == 202) {
          return true;
        } else {
          return false;
        }
      })
      .catch((error) => {
        console.log("ERROR EMAIL", error);
        throw new ErrorException(ErrorCode.SendEmaillError);
      });
    console.log("OUTSIDE THEN CATCH", isEmailSent);
    return isEmailSent;
  }

}
