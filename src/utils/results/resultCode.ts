export class ResultCode {
    public static readonly Created = 'Created';
    public static readonly Post = 'Post';
    public static readonly Updated = 'Updated';
    public static readonly Read = 'Read';
    public static readonly Deleted = 'Deleted';
  }