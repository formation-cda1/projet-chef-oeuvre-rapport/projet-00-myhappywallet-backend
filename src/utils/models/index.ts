import { ResponseOperationFixeGet } from './responseGetOperationFixe.sw.model';
import { RespUpdateDelete } from './responseUpdateDelete.swg.model';
import { ResponseOperationFixeGetById } from './responseGetByIdOperationFixe.sw.model';
import { RequestOperationFixe } from './requestOperationFixe.swg.model';
import {responseOperationFixePost} from './responsePostOperationFixe.sw.model'

export {RequestOperationFixe,responseOperationFixePost, ResponseOperationFixeGet, ResponseOperationFixeGetById, RespUpdateDelete }